import React from 'react'
import './App.css'
import Slider from './components/slider'
import { SLIDES } from './constants'

function App () {
  return (
    <div className='App'>
      <h1 className='logo' style={{ padding: '0 4%' }}>FLETNIX</h1>
      <h2 style={{ padding: '0 4%' }}>Continue Watching</h2>
      <Slider slides={SLIDES} />
      <br />
      <br />
      <h2 style={{ padding: '0 4%' }}>New on Netflix</h2>
      <Slider slides={SLIDES} />
      <br />
      <br />
      <h2 style={{ padding: '0 4%' }}>Crime & Thriller</h2>
      <Slider slides={SLIDES} />
      <br />
      <br />
    </div>
  )
}

export default App
