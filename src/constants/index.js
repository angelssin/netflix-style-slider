export const SLIDES = [
  {
    title: 'The Thick of It',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-1.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABZthb2pnfAy042KW_H_YlXSbKoyICmnwtb52EPbEB9Uv6J2GcYj3c5QKc5pUSkMHZkS6cFe-js7c_qyYjwkdnd9OOmE.webp?r=435'
  },
  {
    title: 'La Revolution',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-2.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABYQUXbRg_zOBnRKj5Xw29SpcMfHg2jecRJ6btj2Use8se7XrcAaFTmZJkbXz1sNjmp1nCV6z_4puzDN-eRbzMq2CcNI6X4PkE7GC4W7sioBTq7wb24P_fe9ceKmT.jpg?r=bf1'
  },
  {
    title: 'The Witcher',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-3.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABelflbF7hLllQWMP9vusyHxWklYqi-_xMHvad3rT2_mVkUWZoD_C1iDbNUCeeZ92dfng-rikd9HvJIUvZZ3oo32fhhKlwEdPt1CyR3cT7occykhrEeKHaHJglR78.jpg?r=382'
  },
  {
    title: 'Misfits',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-4.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABfrDrvEF1qr1QyZ0AVmvI3uj_Qh6_eFQcUkJSHIUVDoHLFDSwicLGmXaPOjcfsqAkyO0G4jzft0FsfSskphdz8Vqjcs.webp?r=41f'
  },
  {
    title: 'Red Dwarf',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-5.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABZMDTyRGuYV5U_PJFS0Wae6jNoW4QORydsTcyd8Q8lPwgXZpz2bJjbZD_6wrsVGPa07TtrP_GH2QKLsIpSUeX4t7swA.webp?r=568'
  },
  {
    title: 'Snowpiercer',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-6.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABbkTVv2A45IHm7IG_xLgMB4sbPCArUtVakpc1DqK44SNHbxRJW1X44LaIQDq0le89Slpl0GA3JTQKh65tXtVRRP3uiSgTTZFTWFTw16uS4NNrnRuqsnUmgxlFAfh.jpg?r=d31'
  },
  {
    title: 'Avatar: The Last Airbender',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-7.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABR3tWi_x4S_BwzbuA-n08sa8Ca2_aIqIpoREVWIeQfFL-GvYxjTkW8NQNXCajLwGpk0ddmyqTfjx8Pt-vXV5j11_RAE.webp?r=b84'
  },
  {
    title: 'Love Death + Robots',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-8.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABchJYaTX9g57exhGt5Lttqb-d6TwbOOjN2iRje2L25jnCvOrBJSK7fRrgLQm6t23KkXSULKbTkAm63OmhOF4FxRAWGvvMeJLgLMBzNC2nHwO1aXufslwO2659No6.jpg?r=739'
  },
  {
    title: 'Watchmen',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-9.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABQrl06NMNGyM3kb7zKJUpWXSq7kBW02f3rG30jTG6_ijzfVyFQ0PHzkvzCk-Qc259x1GzEkIB5-AS0rFrQxP7WZdI2s.webp?r=ab3'
  },
  {
    title: 'Dark',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-10.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABSEl8LWx4tMJIM9Atm3F1Y49Uq6X01tnDe8gPA6d84-gQ767saz9z7Jxj9sFozuI8bcM2vlxeP9IPq3Aa7jxLlkMu8JGjizRLblNEcmD7g-Z2NeZvkvV5nWF9DmJ.jpg?r=393'
  },
  {
    title: 'Dragon\'s Dogma',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-11.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABSGBOo15BD2xaymdCcZmansKBo8QL4cZUzcYbyK50Pr8g2parOqt6w2ypo7efnLthTRNCKl3WEPFIwmUKgavqXKynBMuX_aOPNmtPwKycnIT60Jv7n_Me072fb4l.jpg?r=73e'
  },
  {
    title: 'Marco Polo',
    description: 'A fantasy tv series based on the Witcher series of books by Andrzej Sapkowski follows the stories of Geralt of Rivia',
    url: 'http://slide-12.com',
    image: 'https://occ-0-2007-299.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABUIxGuQrFBA7VK6vd0Iwe09m2nKSsSc3VNlwopkooS97D_PMoGkfyXdlwf6cS9zef_MdtJz0FoRTTSM-3Xj8xQM5WESer2f_MkDa_k9gjY4I7l-STjuhEVuu7sIA.jpg?r=2a8'
  }
]
