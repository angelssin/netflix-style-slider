export function getCoords (cardRef) {
  return cardRef.current.getBoundingClientRect()
}

export function isFirstCardInView (cardIndex, slidesInView) {
  if (cardIndex === 0) return true // Avoid dividing by zero!
  if (cardIndex % slidesInView === 0) return true
}

export function isLastCardInView (cardIndex, slidesInView, totalSlides) {
  if (cardIndex === (totalSlides - 1)) return true
  if (cardIndex % slidesInView === (slidesInView - 1)) return true
}
