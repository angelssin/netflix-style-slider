import styled from 'styled-components'

export const StyledCard = styled.a`
  position: relative;
  display: block;
  width: 100%;
  padding: 28.125% 0; // 16:9 taken directly from netflix
  overflow: hidden;
  border-radius: 4px;
`

export const StyledImage = styled.img`
  width: 100%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`
