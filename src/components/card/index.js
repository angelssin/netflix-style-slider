import React, { useState, useRef } from 'react'
import ExpandedCard from '../expanded_card'
import { getCoords, isFirstCardInView, isLastCardInView } from './helpers'
import { StyledCard, StyledImage } from './styles'

const Card = (props) => {
  const cardRef = useRef()
  const [cardExpanded, setCardExpanded] = useState(false)
  const [isHovered, setIsHovered] = useState(false)
  const [coords, setCoords] = useState({ top: 0, left: 0, width: 0 })
  let mouseEnterTimeout

  function handleMouseEnter () {
    mouseEnterTimeout = setTimeout(handleExpandedCard, 300)
  }

  function handleMouseLeave () {
    if (cardExpanded) return
    clearTimeout(mouseEnterTimeout)
  }

  function handleExpandedCard () {
    const { top, left, width } = getCoords(cardRef)
    const topWithScrollPos = top + window.scrollY

    setCoords({ top: topWithScrollPos, left, width })
    setCardExpanded(true)
    setIsHovered(true)
  }

  function handleCollapseCard () {
    setCardExpanded(false)
  }

  function renderExpandedCard () {
    if (!cardExpanded) return null

    const { cardIndex, slidesInView, totalSlides } = props
    return (
      <ExpandedCard
        isFirstCardInView={isFirstCardInView(cardIndex, slidesInView)}
        isLastCardInView={isLastCardInView(cardIndex, slidesInView, totalSlides)}
        coords={coords}
        handleCollapseCard={handleCollapseCard}
        isOpen={cardExpanded}
        isHovered={isHovered}
        setIsHovered={setIsHovered}
        cardData={props.cardData}
      />
    )
  }

  return (
    <StyledCard
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      href={props.cardData.href}
      ref={cardRef}
    >
      <StyledImage src={props.cardData.image} />
      {renderExpandedCard()}
    </StyledCard>
  )
}

export default Card
