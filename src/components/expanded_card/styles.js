import styled from 'styled-components'

export const StyledImage = styled.img`
  width: 100%;
`

export const StyledContent = styled.div`
  padding: 0.6rem;
`

export const StyledTitle = styled.h3`
  margin-top: 0;
  font-size: 1rem;
`

export const StyledDescription = styled.p`
  color: white;
  line-height: 1.5;
  font-size: 0.6rem;
`

export const StyledContainer = styled.div`
  overflow: hidden;
  position: absolute;
  background: #181818;
  box-shadow: rgba(0,0,0,0.75) 0 3px 10px;
  color: white;
  font-size: 16px;
  border-radius: 4px;
  transform-origin: center center;
  opacity: 1;
  transform: scale(1);
  transition: transform 0.2s ease-out, opacity 0.2s ease-in;

  ${({ isFirstCardInView }) => isFirstCardInView && `
    transform-origin: left;
  `}

  ${({ isLastCardInView }) => isLastCardInView && `
    transform-origin: right;
  `}

  &.expanded-card-enter-active,
  &.expanded-card-enter-done {
    opacity: 1;
    transform: scale(1.5);
    transition: transform 0.2s ease-out, opacity 0s;
  }

  &.expanded-card-exit-active {
    opacity: 0.2;
  }
`
