import React from 'react'
import ReactDom from 'react-dom'
import { CSSTransition } from 'react-transition-group'
import {
  StyledContainer,
  StyledImage,
  StyledContent,
  StyledTitle,
  StyledDescription
} from './styles'

function ExpandedCard (props) {
  if (!props.isOpen) return null

  function handleMouseLeave () {
    props.setIsHovered(false)
  }

  function handleExit () {
    props.handleCollapseCard()
  }

  return ReactDom.createPortal(
    <CSSTransition
      onExited={handleExit}
      classNames='expanded-card'
      in={props.isHovered}
      timeout={200}
    >
      <StyledContainer
        isFirstCardInView={props.isFirstCardInView}
        isLastCardInView={props.isLastCardInView}
        onMouseLeave={handleMouseLeave}
        style={{
          top: props.coords.top,
          left: props.coords.left,
          width: props.coords.width
        }}
      >
        <StyledImage src={props.cardData.image} />
        <StyledContent>
          <StyledTitle>{props.cardData.title}</StyledTitle>
          <StyledDescription>{props.cardData.description}</StyledDescription>
        </StyledContent>
      </StyledContainer>
    </CSSTransition>,
    document.getElementById('portal')
  )
}

export default ExpandedCard
