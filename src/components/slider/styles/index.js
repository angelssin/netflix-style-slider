import styled from 'styled-components'

export const StyledSliderWrapper = styled.div`
  overflow-x: hidden;
`

export const StyledSlider = styled.div`
  position: relative;
  padding: 0 4%;
  touch-action: pan-y; // Stops user from panning up and down?
`

export const StyledSliderMask = styled.div`
  line-height: 0;
  overflow-x: visible;
`

export const StyledSliderContent = styled.div`
  transition: transform .75s ease-in-out 0s;
  white-space: nowrap;
`
