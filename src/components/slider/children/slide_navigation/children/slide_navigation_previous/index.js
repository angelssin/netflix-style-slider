import React from 'react'
import PropTypes from 'prop-types'
import SlideNavigation from '../..'
import { StyledContainer } from './styles'
import { FIRST_SLIDE } from './constants'

const SlideNavigationPrevious = (props) => {
  const goToPreviousSlide = (event) => {
    event.preventDefault()
    if (props.currentSlide === FIRST_SLIDE) return
    const previousSlide = props.currentSlide - 1
    console.log('setting previous slide', previousSlide)
    props.setCurrentSlide(previousSlide)
  }

  if (props.currentSlide === FIRST_SLIDE) return null

  return (
    <StyledContainer>
      <SlideNavigation handleGoToSlide={goToPreviousSlide}>
        {'( < )'}
      </SlideNavigation>
    </StyledContainer>
  )
}

SlideNavigationPrevious.propTypes = {
  currentSlide: PropTypes.number,
  setCurrentSlide: PropTypes.func
}

export default SlideNavigationPrevious
