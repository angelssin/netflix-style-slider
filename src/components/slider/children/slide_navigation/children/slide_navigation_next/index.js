import React from 'react'
import PropTypes from 'prop-types'
import SlideNavigation from '../..'
import { StyledContainer } from './styles'

const SlideNavigationNext = (props) => {
  const goToNextSlide = (event) => {
    event.preventDefault()
    if (props.currentSlide === props.lastSlide) return
    const nextSlide = props.currentSlide + 1
    console.log('setting next slide', nextSlide)
    props.setCurrentSlide(nextSlide)
  }

  return (
    <StyledContainer>
      <SlideNavigation handleGoToSlide={goToNextSlide}>
        {'( > )'}
      </SlideNavigation>
    </StyledContainer>
  )
}

SlideNavigationNext.propTypes = {
  currentSlide: PropTypes.number,
  setCurrentSlide: PropTypes.func
}

export default SlideNavigationNext
