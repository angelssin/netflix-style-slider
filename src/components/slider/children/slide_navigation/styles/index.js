import styled from 'styled-components'

export const StyledButton = styled.button`
  box-sizing: border-box;
  height: 100%;
  width: 100%;
  cursor: pointer;
  padding: 0;
  line-height: 0;
  border: none;
  outline: none;
  font-weight: bold;
  background: rgba(0,0,0, 0.5);
  color: white;
`
