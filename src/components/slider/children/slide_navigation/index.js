import React from 'react'
import { StyledButton } from './styles'

const SlideNavigation = (props) => {
  return (
    <StyledButton onClick={props.handleGoToSlide}>
      {props.children}
    </StyledButton>
  )
}

export default SlideNavigation
