import styled from 'styled-components'

// const SLIDER_WIDTH = 100 // representing 100%
// const getSlideWidth = (slidesInView) => {
//   return SLIDER_WIDTH / slidesInView
// }

export const StyledSlide = styled.div`
  /* TODO: Add media queries ??? */
  box-sizing: border-box;
  display: inline-block;
  padding: 0 2px;
  width: ${props => props.width}%;
  
  &:first-child {
    padding-left: 0;
  }
`
