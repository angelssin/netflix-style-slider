import React from 'react'
import PropTypes from 'prop-types'
import { StyledSlide } from './styles'

const Slide = (props) => {
  return (
    <StyledSlide width={props.width}>
      {props.children}
    </StyledSlide>
  )
}

Slide.propTypes = {
  width: PropTypes.number
}

export default Slide
