import { ONE_HUNDRED_PERCENT } from '../constants'

export const getLeftPosition = (currentSlide, slideWidth) => {
  return `-${currentSlide * (ONE_HUNDRED_PERCENT)}%`
}

export const getSlideWidth = (slidesInView) => {
  return ONE_HUNDRED_PERCENT / slidesInView
}

export const getLastSlideNumber = (cardTotal, slidesInView) => {
  return Math.ceil(cardTotal / slidesInView) - 1
}
