import React, { useState } from 'react'
import PropTypes from 'prop-types'
import Card from '../card'
import Slide from './children/slide'
import SlideNavigationNext from './children/slide_navigation/children/slide_navigation_next'
import SlideNavigationPrevious from './children/slide_navigation/children/slide_navigation_previous'
import {
  StyledSliderWrapper,
  StyledSlider,
  StyledSliderMask,
  StyledSliderContent
} from './styles'
import { getSlideWidth, getLeftPosition, getLastSlideNumber } from './helpers'

const Slider = (props) => {
  const [currentSlide, setCurrentSlide] = useState(0)
  // May need to be controlled by state so we can
  // update it when media query changes
  const slideWidth = getSlideWidth(props.slidesInView)

  const renderSlide = (slideData, index) => {
    return (
      <Slide key={`slide-${index}`} width={slideWidth}>
        <Card
          cardData={slideData}
          cardIndex={index}
          currentSlide={currentSlide}
          slidesInView={props.slidesInView}
          totalSlides={props.slides.length}
        />
      </Slide>
    )
  }

  return (
    <StyledSliderWrapper>
      <StyledSlider>
        <StyledSliderMask>
          <StyledSliderContent style={{ transform: `translate3d(${getLeftPosition(currentSlide, slideWidth)}, 0px, 0px)` }}>
            {props.slides.map(renderSlide)}
          </StyledSliderContent>
        </StyledSliderMask>
        <SlideNavigationNext
          setCurrentSlide={setCurrentSlide}
          currentSlide={currentSlide}
          lastSlide={getLastSlideNumber(props.slides.length, props.slidesInView)}
        />
        <SlideNavigationPrevious
          setCurrentSlide={setCurrentSlide}
          currentSlide={currentSlide}
        />
      </StyledSlider>
    </StyledSliderWrapper>
  )
}

Slider.defaultProps = {
  slidesInView: 6
  // slideBy: 6
}

Slider.propTypes = {
  slidesInView: PropTypes.number
  // slideBy: PropTypes.number
}

export default Slider
